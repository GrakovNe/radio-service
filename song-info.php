<?php
header("Access-Control-Allow-Origin: *");

$artist = exec("mpc -h redh0rse@localhost --format \"[%artist%]\" | head -n 1");
$album  = exec("mpc -h redh0rse@localhost --format \"[%album%]\" | head -n 1");
$title  = exec("mpc -h redh0rse@localhost --format \"[%title%]\" | head -n 1");

$song_data = array (
'title'  => $title,
'album'  => $album,
'artist' => $artist
);

echo json_encode ($song_data);

?>
