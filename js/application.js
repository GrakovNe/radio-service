var RADIO_STREAM = "radio_stream";
var RADIO_URL = "http://grakovne.org:8000";
var RADIO_NAME = "GrakovNe's Web Radio Service";

var VOLUME_LOCAL_STORAGE_KEY = "radio-volume-key";

var to_play_image = "data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4IiB2aWV3Qm94PSIwIDAgNTEwIDUxMCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEwIDUxMDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPgo8Zz4KCTxnIGlkPSJwbGF5LWNpcmNsZS1maWxsIj4KCQk8cGF0aCBkPSJNMjU1LDBDMTE0Ljc1LDAsMCwxMTQuNzUsMCwyNTVzMTE0Ljc1LDI1NSwyNTUsMjU1czI1NS0xMTQuNzUsMjU1LTI1NVMzOTUuMjUsMCwyNTUsMHogTTIwNCwzNjkuNzV2LTIyOS41TDM1NywyNTUgICAgTDIwNCwzNjkuNzV6IiBmaWxsPSIjMDA2REYwIi8+Cgk8L2c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==";
var to_pause_image = "data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4IiB2aWV3Qm94PSIwIDAgNTEwIDUxMCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEwIDUxMDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPgo8Zz4KCTxnIGlkPSJwYXVzZS1jaXJjbGUtb3V0bGluZSI+CgkJPHBhdGggZD0iTTE3OC41LDM1N2g1MVYxNTNoLTUxVjM1N3ogTTI1NSwwQzExNC43NSwwLDAsMTE0Ljc1LDAsMjU1czExNC43NSwyNTUsMjU1LDI1NXMyNTUtMTE0Ljc1LDI1NS0yNTVTMzk1LjI1LDAsMjU1LDB6ICAgICBNMjU1LDQ1OWMtMTEyLjIsMC0yMDQtOTEuOC0yMDQtMjA0UzE0Mi44LDUxLDI1NSw1MXMyMDQsOTEuOCwyMDQsMjA0UzM2Ny4yLDQ1OSwyNTUsNDU5eiBNMjgwLjUsMzU3aDUxVjE1M2gtNTFWMzU3eiIgZmlsbD0iIzAwNkRGMCIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=";

var controller_view = $('#controller-view');

function setSongInfo() {
    $.ajax({
        url: 'https://radio.grakovne.org/song-info.php',
        success: function (data) {
            var songInfo = JSON.parse(data);

            setSongData(songInfo, "title");
            setSongData(songInfo, "album");
            setSongData(songInfo, "artist");

            setPageTitle(songInfo["title"]);
        }
    });
}

function setSongData(tags, tag) {
    if (tags[tag] != '') {
        $("#" + tag).text(tags[tag].toUpperCase());
    } else {
        $("#" + tag).text("NO " + tag.toUpperCase());
    }
}

function getStream() {
    return soundManager.getSoundById(RADIO_STREAM)
}

function setupPlayer() {
    soundManager.debugMode = false;
    soundManager.setup({
        onready: function () {
            createRadioStream();
            setRadioVolume(getSavedVolume());
            playStream();
        }
    });
}

function getSavedVolume() {
    var savedVol = localStorage.getItem(VOLUME_LOCAL_STORAGE_KEY);

    if (savedVol === null) {
        savedVol = 100;
    }

    return savedVol;
}

function setSavedVolume(newVolume) {
    localStorage.setItem(VOLUME_LOCAL_STORAGE_KEY, newVolume);
}

function getPlayState() {
    try {
        switch (getStream().playState) {
            case 0:
                return "paused";
            case 1:
                return "played";
        }
    } catch (e) {
        return "paused";
    }
}

function createRadioStream() {
    return soundManager.createSound({
        id: RADIO_STREAM,
        url: RADIO_URL,
    });
}

function playStream() {
    createRadioStream();
    setRadioVolume(getSavedVolume());
    soundManager.play(RADIO_STREAM, {
        onstop: function () {
            getStream().destruct();
        }
    });

    setSongInfo();
    controller_view.attr('src', to_pause_image);
}

function setPageTitle(songTitle) {
    var newTitle = "";

    if (songTitle != "") {
        newTitle = songTitle + " | ";
    }

    newTitle += RADIO_NAME;

    document.title = newTitle;
}

function stopStream() {
    soundManager.stop(RADIO_STREAM);
    controller_view.attr('src', to_play_image);
}

function showUI() {
    $("#radio-player").show();
}

function getClickedVolume(e) {
    var widthclicked = e.pageX - $("#volume-control").offset().left;
    var totalWidth = $("#volume-control").width(); // can also be cached somewhere in the app if it doesn't change
    var calc = (widthclicked / totalWidth * 100); // get the percent of bar clicked and multiply in by the duration
    return (calc).toFixed(0);
}

function setProgressBarVolume(volume) {
    $("#volume-control-child").css("width", volume + "%");
}

function setRadioVolume(volume) {
    setSavedVolume(volume);
    getStream().setVolume(volume);
    setProgressBarVolume(volume);

}

$("#controller-view").click(function () {
    switch (getPlayState()) {
        case "paused":
            playStream();
            break;

        case "played":
            stopStream();
            break;
    }
});

$("#volume-control").click(function (e) {
    if (getPlayState() === "played") {
        setRadioVolume(getClickedVolume(e));
    }
});

setupPlayer();
setSongInfo();
showUI();

setInterval(setSongInfo, 5000);


